const mongoose = require('mongoose')
const nanoid = require('nanoid')

const VerificationTokenSchema = new mongoose.Schema({
  created: {
    type: Number,
    default: Date.now,
  },
  token: {
    type: String,
    default: nanoid, //TODO: collisions are possible and should be handled
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  verifies: {
    type: String,
    enum: ['email', 'phone', 'password'],
    required: true,
  },
})

const VerificationToken = mongoose.model('VerificationToken', VerificationTokenSchema)

module.exports = VerificationToken
