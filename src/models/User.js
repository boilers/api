const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')
const salt = bcrypt.genSaltSync(10)

const UserSchema = new mongoose.Schema({
  created: {
    type: Number,
    default: Date.now,
  },
  sessions: {
    type: [String],
    select: false,
  },
  email: {
    type: String,
    required: true,
  },
  emailVerified: {
    type: Number,
    default: 0,
  },
  password: {
    type: String,
    required: true,
    select: false,
  },
  phone: {
    type: String,
  },
  phoneVerified: {
    type: Number,
    default: 0,
  },
  firstname: {
    type: String,
  },
  lastname: {
    type: String,
  },
  address: {
    type: String,
  },
  city: {
    type: String,
  },
  zipcode: {
    type: String,
  },
})

// Hash password
UserSchema.pre('save', function (next) {
  if (!this.isModified('password')) return next()

  const { password } = this
  const hashed = bcrypt.hashSync(password, salt)
  this.password = hashed

  next()
})

UserSchema.methods.comparePassword = function (password) {
  const result = bcrypt.compareSync(password, this.password)
  this.password = undefined // do not leak password
  return result
}

const User = mongoose.model('User', UserSchema)

module.exports = User
