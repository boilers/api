const router = require('express').Router()
const Users = require('../controllers/users')
const User = require('../models/User')

const {
  INTERNAL_SERVER_ERROR,
} = require('../utils/responses')

router.post('/sign-up', Users.signUp)
router.post('/sign-in', Users.signIn)
router.post('/sign-out', Users.authenticate, Users.signOut)
router.get('/sign-out-all', Users.signOutAll)
router.get('/authenticate', Users.authenticate, async (req, res) => {
  try {
    const me = req.me
    const user = me && await User.findById(me.id)
    if (!user) {
      return res.json(null)
    }

    res.json(user)
  } catch (error) {
    const { code } = INTERNAL_SERVER_ERROR
    res.status(code).json(INTERNAL_SERVER_ERROR)
  }
})

router.put('/update', Users.authenticate, Users.update)

router.put('/update-email', Users.authenticate, Users.updateEmail)
router.get('/verify-email', Users.verifyEmail)
router.get('/request-verify-email', Users.authenticate, Users.requestVerifyEmail)

router.put('/update-phone', Users.authenticate, Users.updatePhone)
router.get('/verify-phone', Users.verifyPhone)

router.put('/update-password', Users.authenticate, Users.updatePassword)
router.post('/reset-password', Users.resetPassword)
router.post('/request-reset-password', Users.requestResetPassword)

module.exports = router
