const redis = require('redis')
const { promisify } = require('util')

const {
  REDIS_HOST,
  REDIS_USER,
  REDIS_PASSWORD,
  REDIS_PORT,
} = process.env

const clientOptions = {
  host: REDIS_HOST,
  user: REDIS_USER,
  port: REDIS_PORT,
}
const { NODE_ENV } = process.env
if (NODE_ENV !== 'development') {
  clientOptions.password =  REDIS_PASSWORD
}

const client = redis.createClient(clientOptions)
client.on('connect', () => {
  console.log(`Connected to Redis.`)
})

const set = promisify(client.set).bind(client)
const get = promisify(client.get).bind(client)
const del = promisify(client.del).bind(client)

module.exports = {
  set,
  get,
  del,
}
