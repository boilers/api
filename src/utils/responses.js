function missingParameters (params, message) {
  const missing = Object.entries(params).map(([key, value]) => {
    if (value === undefined) return key
  }).filter(_ => _)

  if (!missing.length) return null
  return {
    code: 400,
    message: `${message}: ${missing.join(', ')}.`
  }
}

function missingOptional (params) {
  return missingParameters(params, 'Missing optional parameter(s)')
}
function missingRequired (params) {
  return missingParameters(params, 'Missing required parameter(s)')
}

const INTERNAL_SERVER_ERROR = {
  code: 500,
  message: 'Internal server error.',
}

const BAD_REQUEST = {
  code: 400,
  message: `Bad request.`,
}

const UNAUTHORIZED = {
  code: 401,
  message: `Unauthorized request.`,
}

const EMAIL_INVALID = {
  code: 400,
  message: 'Invalid e-mail.',
}

const EMAIL_EXISTS = {
  code: 422,
  message: 'E-mail is already registered.',
}

const EMAIL_PASSWORD_NO_MATCH = {
  code: 401,
  message: `E-mail and password combination doesn't match.`,
}

const PASSWORDS_NO_MATCH = {
  code: 422,
  message: 'Passwords do not match.',
}

module.exports = {
  missingOptional,
  missingRequired,
  INTERNAL_SERVER_ERROR,
  BAD_REQUEST,
  UNAUTHORIZED,
  EMAIL_INVALID,
  EMAIL_EXISTS,
  EMAIL_PASSWORD_NO_MATCH,
  PASSWORDS_NO_MATCH,
}
