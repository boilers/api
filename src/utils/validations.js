module.exports = {
  validateEmail (input) {
    const regex = /[\w-.]+@[\w-]+[.][\w-]+([.][\w-]+)?/
    return regex.test(input)
  }
}
