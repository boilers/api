function logger (req, res, next) {
  console.log('\n')

  const { method, url } = req
  console.log(`${method}: ${url}`)

  if (Object.keys(req.query).length) {
    console.log('QUERY:')
    console.log(JSON.stringify(req.query, null, 2))
  }
  if (Object.keys(req.body).length) {
    console.log('BODY:')
    console.log(JSON.stringify(req.body, null, 2))
  }

  next()
}

module.exports = logger
