const mongoose = require('mongoose')
const {
  MONGO_HOST,
  MONGO_USER,
  MONGO_PASSWORD,
  MONGO_PORT,
  MONGO_DATABASE,
  NODE_ENV,
} = process.env

let url
if (NODE_ENV === 'development') {
  url = `mongodb://${MONGO_HOST}:${MONGO_PORT}/${MONGO_DATABASE}`
} else {
  url = `mongodb://${MONGO_USER}:${MONGO_PASSWORD}@${MONGO_HOST}:${MONGO_PORT}/${MONGO_DATABASE}`
}
mongoose.connect(url)

mongoose.connection.on('connected', () => {
  console.log(`Connected to MongoDB.`)
})

module.exports = mongoose
