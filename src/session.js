const redis = require('./redis')

async function set (token, data) {
  const sessionData = JSON.stringify(data)
  return redis.set(token, sessionData)
}

async function get (token) {
  const sessionData = await redis.get(token)
  return JSON.parse(sessionData)
}

async function del (token) {
  return set(token, {})
}

module.exports = {
  set,
  get,
  del,
}
