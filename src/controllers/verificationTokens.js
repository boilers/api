const VerificationToken = require('../models/VerificationToken')

const {
  INTERNAL_SERVER_ERROR,
  BAD_REQUEST,
  UNAUTHORIZED,
} = require('../utils/responses')

class VerificationTokens {
  static async create (userId, verifies) {
    try {
      if (!userId || !verifies) {
        throw new Error('Missing arguments.')
      }

      // TODO: probably check if token already exists so we don't accidentaly hijack another users account
      const token = await VerificationToken.create({ userId, verifies })
      if (token) {
        return token
      }
    } catch (error) {
      return null
    }
  }

  static async find (token) {
    try {
      if (!token) {
        throw new Error('Missing token.')
      }
      const exists = await VerificationToken.findOne({ token })
      if (!exists) {
        return null
      }
      return exists
    } catch (error) {
      return null
    }
  }

  static async findByUserId (userId, verifies) {
    try {
      const verificationToken = await VerificationToken.findOne({ userId, verifies })
      if (!verificationToken) {
        return null
      }

      return verificationToken
    } catch (error) {
      return null
    }
  }
}

module.exports = VerificationTokens
