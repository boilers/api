const User = require('../models/User')
const VerificationTokens = require('./verificationTokens')
const session = require('../session')
const { uuidv4 } = require('../utils')

const {
  missingOptional,
  missingRequired,
  INTERNAL_SERVER_ERROR,
  UNAUTHORIZED,
  EMAIL_INVALID,
  EMAIL_EXISTS,
  EMAIL_PASSWORD_NO_MATCH,
  PASSWORDS_NO_MATCH,
} = require('../utils/responses')

const {
  validateEmail,
} = require('../utils/validations')

const { NODE_ENV } = process.env
const cookieConfig = {
  httpOnly: true,
  secure: NODE_ENV !== 'development',
}

class Users {
  static async signUp (req, res) {
    try {
      const { email, password, passwordRepeat } = req.body
      let ERROR = missingRequired({ email, password, passwordRepeat })
      if (ERROR) return res.status(ERROR.code).json(ERROR)

      if (password !== passwordRepeat) {
        const { code } = PASSWORDS_NO_MATCH
        return res.status(code).json(PASSWORDS_NO_MATCH)
      }

      const emailIsValid = validateEmail(email)
      if (!emailIsValid) {
        const { code } = EMAIL_INVALID
        return res.status(code).json(EMAIL_INVALID)
      }

      const emailExists = await User.findOne({ email })
      if (emailExists) {
        const { code } = EMAIL_EXISTS
        return res.status(code).json(EMAIL_EXISTS)
      }

      const user = await User.create({ email, password })

      const verificationToken = await VerificationTokens.create(user._id, 'email')
      console.log(`http://0.0.0.0:8080/verify/${verificationToken.token}`)
      // TODO: send e-mail with verification link

      Users.signIn(req, res)
    } catch (error) {
      const { code } = INTERNAL_SERVER_ERROR
      res.status(code).json(INTERNAL_SERVER_ERROR)
    }
  }

  static async signIn (req, res) {
    try {
      const { email, password } = req.body
      let ERROR = missingRequired({ email, password })
      if (ERROR) return res.status(ERROR.code).json(ERROR)

      let user = await User.findOne({ email }).select('+password +sessions')
      if (!user) {
        const { code } = EMAIL_PASSWORD_NO_MATCH
        return res.status(code).json(EMAIL_PASSWORD_NO_MATCH)
      }

      const match = user.comparePassword(password)
      if (!match) {
        const { code } = EMAIL_PASSWORD_NO_MATCH
        return res.status(code).json(EMAIL_PASSWORD_NO_MATCH)
      }

      const token = uuidv4()
      await session.set(token, { id: user._id })
      res.cookie('token', token, cookieConfig)
      const sessionTokens = [token].concat(user.sessions)
      user = await User.findOneAndUpdate({ _id: user._id }, { sessions: sessionTokens }, { new: true })
      user.sessions = undefined // do not leak sessions

      res.json(user)
    } catch (error) {
      const { code } = INTERNAL_SERVER_ERROR
      res.status(code).json(INTERNAL_SERVER_ERROR)
    }
  }

  static async signOut (req, res) {
    try {
      const { token } = req.cookies
      const me = req.me

      const user = await User.findById(me.id, '+sessions')
      if (user) {
        let sessionTokens = user.sessions
        sessionTokens = sessionTokens.filter(t => t !== token)
        user.sessions = sessionTokens
        user.save()
      }

      await session.del(token)
      res.clearCookie('token')

      res.status(201).end()
    } catch (error) {
      const { code } = INTERNAL_SERVER_ERROR
      res.status(code).json(INTERNAL_SERVER_ERROR)
    }
  }

  static async signOutAll (req, res) {
    try {
      let token
      if (req.query.token) token = req.query.token
      else if (req.cookies.token) token = req.cookies.token
      if (!token) {
        const { code } = UNAUTHORIZED
        return res.status(code).json(UNAUTHORIZED)
      }

      const user = await User.findOne({ sessions: token }, '+sessions')
      if (!user) {
        const { code } = UNAUTHORIZED
        return res.status(code).json(UNAUTHORIZED)
      }

      const tokens = user.sessions
      await session.del(tokens.join(' '))
      res.clearCookie('token')
      user.sessions = []
      user.save()

      res.status(201).end()
    } catch (error) {
      const { code } = INTERNAL_SERVER_ERROR
      res.status(code).json(INTERNAL_SERVER_ERROR)
    }
  }

  static async authenticate (req, res, next) {
    try {
      const { token } = req.cookies
      if (token) req.me = await session.get(token)
      next()
    } catch (error) {
      const { code } = INTERNAL_SERVER_ERROR
      res.status(code).json(INTERNAL_SERVER_ERROR)
    }
  }

  // TODO: refactor to factory function?
  static authorize (permissions = []) {
    try {
      // TODO: check permissions or throw UNAUTHORIZED
    } catch (error) {
      const { code } = INTERNAL_SERVER_ERROR
      res.status(code).json(INTERNAL_SERVER_ERROR)
    }
  }

  static async update (req, res) {
    try {
      const me = req.me
      if (!me) {
        const { code } = UNAUTHORIZED
        return res.status(code).json(UNAUTHORIZED)
      }

      const {
        firstname, lastname,
        address, city, zipcode,
      } = req.body

      const updates = {}
      if (firstname) updates.firstname = firstname
      if (lastname) updates.lastname = lastname

      if (address) updates.address = address
      if (city) updates.city = city
      if (zipcode) updates.zipcode = zipcode

      const user = await User.findOneAndUpdate({ _id: me.id }, updates, { new: true })
      res.json(user)
    } catch (error) {
      const { code } = INTERNAL_SERVER_ERROR
      res.status(code).json(INTERNAL_SERVER_ERROR)
    }
  }

  static async updateEmail (req, res) {
    try {
      const me = req.me
      if (!me) {
        const { code } = UNAUTHORIZED
        return res.status(code).json(UNAUTHORIZED)
      }

      const { email, password } = req.body
      let ERROR = missingRequired({ email, password })
      if (ERROR) return res.status(ERROR.code).json(ERROR)

      const emailIsValid = validateEmail(email)
      if (!emailIsValid) {
        const { code } = EMAIL_INVALID
        return res.status(code).json(EMAIL_INVALID)
      }

      const emailExists = await User.findOne({ email })
      if (emailExists) {
        const { code } = EMAIL_EXISTS
        return res.status(code).json(EMAIL_EXISTS)
      }

      const user = await User.findById(me.id, '+password')
      const match = user.comparePassword(password)
      if (!match) {
        const { code } = EMAIL_PASSWORD_NO_MATCH
        return res.status(code).json(EMAIL_PASSWORD_NO_MATCH)
      }

      // TODO: user.email = email, user.save() doesn't work without password, investigate and fix
      const updates = { email, emailVerified: 0 }
      const updated = await User.findOneAndUpdate({ _id: me.id }, updates, { new: true })

      const verificationToken = await VerificationTokens.create(me.id, 'email')
      console.log(`http://0.0.0.0:8080/verify/${verificationToken.token}`)
      // TODO: send e-mail with verification link

      res.json({
        email: updated.email,
        emailVerified: updated.emailVerified
      })
    } catch (error) {
      const { code } = INTERNAL_SERVER_ERROR
      res.status(code).json(INTERNAL_SERVER_ERROR)
    }
  }

  static async verifyEmail (req, res) {
    try {
      const { token } = req.query
      let ERROR = missingRequired({ token })
      if (ERROR) return res.status(ERROR.code).json(ERROR)

      const verificationToken = await VerificationTokens.find(token)
      if (!verificationToken) {
        const { code } = UNAUTHORIZED
        return res.status(code).json(UNAUTHORIZED)
      }

      await User.findOneAndUpdate({ _id: verificationToken.userId }, { emailVerified: 1 })
      await verificationToken.remove() // TODO: void this instead of removing to be able to analyze the data?

      res.json({ emailVerified: 1 })
    } catch (error) {
      const { code } = INTERNAL_SERVER_ERROR
      res.status(code).json(INTERNAL_SERVER_ERROR)
    }
  }

  static async requestVerifyEmail (req, res) {
    try {
      const me = req.me
      if (!me) {
        const { code } = UNAUTHORIZED
        return res.status(code).json(UNAUTHORIZED)
      }

      let verificationToken = await VerificationTokens.findByUserId(me.id, 'email')
      if (!verificationToken) {
        // TODO: if token expiry is implemented, create a new one if the old one is invalid
        verificationToken = await VerificationTokens.create(me.id, 'email')
      }
      console.log(`http://0.0.0.0:8080/verify/${verificationToken.token}`)
      // TODO: send e-mail with verification link

      res.status(201).end()
    } catch (error) {
      const { code } = INTERNAL_SERVER_ERROR
      res.status(code).json(INTERNAL_SERVER_ERROR)
    }
  }

  static async updatePhone (req, res) {
    try {
      const me = req.me
      if (!me) {
        const { code } = UNAUTHORIZED
        return res.status(code).json(UNAUTHORIZED)
      }
      // TODO: set phoneVerified = 0
      const verificationToken = await VerificationTokens.create(me.id, 'phone')
    } catch (error) {
      const { code } = INTERNAL_SERVER_ERROR
      res.status(code).json(INTERNAL_SERVER_ERROR)
    }
  }

  static async verifyPhone (req, res) {
    try {
      const { token } = req.query
      let ERROR = missingRequired({ token })
      if (ERROR) return res.status(ERROR.code).json(ERROR)

      // TODO: set user.phoneVerified = 1
    } catch (error) {
      const { code } = INTERNAL_SERVER_ERROR
      res.status(code).json(INTERNAL_SERVER_ERROR)
    }
  }

  static async updatePassword (req, res) {
    try {
      const me = req.me
      if (!me) {
        const { code } = UNAUTHORIZED
        return res.status(code).json(UNAUTHORIZED)
      }

      const { password, newPassword, newPasswordRepeat } = req.body
      let ERROR = missingRequired({ password, newPassword, newPasswordRepeat })
      if (ERROR) return res.status(ERROR.code).json(ERROR)

      if (newPassword !== newPasswordRepeat) {
        const { code } = PASSWORDS_NO_MATCH
        return res.status(code).json(PASSWORDS_NO_MATCH)
      }

      const user = await User.findById(me.id, '+password')
      const match = user.comparePassword(password)
      if (!match) {
        const { code } = EMAIL_PASSWORD_NO_MATCH
        return res.status(code).json(EMAIL_PASSWORD_NO_MATCH)
      }

      user.password = newPassword
      user.save()

      res.status(201).end()
    } catch (error) {
      const { code } = INTERNAL_SERVER_ERROR
      res.status(code).json(INTERNAL_SERVER_ERROR)
    }
  }

  static async requestResetPassword (req, res) {
    try {
      const { emailOrPhone } = req.body
      let ERROR = missingRequired({ emailOrPhone })
      if (ERROR) return res.status(ERROR.code).json(ERROR)

      // TODO: currently only email is implemented, use regex to figure out if e-mail or phone should be used
      const email = emailOrPhone
      let query = {}
      if (email) query.email = email
      else if (phone) query.phone = phone

      const user = await User.findOne(query)
      if (!user) {
        // return success to prevent abuse
        return res.status(201).end()
      }

      const verificationToken = await VerificationTokens.create(user._id, 'password')
      if (email) {
        // TODO: Send e-mail containing link with token
        console.log('Send e-mail.', verificationToken.token)
      } else if (phone) {
        // TODO: Send SMS containing link with token
        console.log('Send SMS.', verificationToken.token)
      }
      console.log(`http://0.0.0.0:8080/reset-password/${verificationToken.token}`)

      return res.status(201).end()
    } catch (error) {
      const { code } = INTERNAL_SERVER_ERROR
      res.status(code).json(INTERNAL_SERVER_ERROR)
    }
  }

  static async resetPassword (req, res) {
    try {
      const { resetPasswordToken,  newPassword, newPasswordRepeat } = req.body
      let ERROR = missingRequired({ resetPasswordToken, newPassword, newPasswordRepeat })
      if (ERROR) return res.status(ERROR.code).json(ERROR)

      const verificationToken = await VerificationTokens.find(resetPasswordToken)
      if (!verificationToken) {
        const { code } = UNAUTHORIZED
        return res.status(code).json(UNAUTHORIZED)
      }

      if (newPassword !== newPasswordRepeat) {
        const { code } = PASSWORDS_NO_MATCH
        return res.status(code).json(PASSWORDS_NO_MATCH)
      }

      const user = await User.findById(verificationToken.userId)
      if (!user) {
        // TODO: handle the unlikely but potential case where no matching user is found?
        throw new Error('VerificationToken could not be matched to a user.')
      }

      user.password = newPassword
      user.save()
      await verificationToken.remove() // TODO: void this instead of removing to be able to analyze the data?

      // replace request body with sign-in credentials
      req.body = {
        email: user.email,
        password: newPassword
      }
      Users.signIn(req, res)
    } catch (error) {
      const { code } = INTERNAL_SERVER_ERROR
      res.status(code).json(INTERNAL_SERVER_ERROR)
    }
  }
}

module.exports = Users
