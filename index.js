const redis = require('./src/redis')
const mongo = require('./src/mongo')

const express = require('express')
const app = express()

const { HOST, PORT, NODE_ENV } = process.env

if (NODE_ENV === 'development') {
  app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://0.0.0.0:8080')
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE')
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type')
    res.setHeader('Access-Control-Allow-Credentials', true)

    next()
  })
}

const cookieParser = require('cookie-parser')
app.use(cookieParser())

const bodyParser = require('body-parser')
app.use(bodyParser())

if (NODE_ENV === 'development') {
  const logger = require('./src/middleware/logger')
  app.use(logger)
}

const router = require('./src/routes')
app.use(router)

app.listen(PORT, HOST, () => console.log(`API running on http://${HOST}:${PORT}`))
