# API
## Getting started
Clone the repository.
```bash
git clone https://gitlab.com/boilers/api.git
cd api
```
Copy .env-example.
```bash
cp .env-example .env
```
Install dependencies and build docker image.
```bash
npm install
docker build .
```
Start the development environment.
```bash
docker-compose up
```
